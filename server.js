var express = require('express')
var app = express()

app.set('port',3000)
// app.use('/public',express.static(__dirname+'/public'))
// app.use('/bower_components',express.static(__dirname+'/bower_components'))

app.get('/',function(req,res){
    res.sendFile('index.html',{root:'./public'})
})

app.listen(app.get('port'),function(err){
    if(err){
        console.log("Error Occured")
    } else {
        console.log("Listening on the PORT "+app.get('port'))
    }
})